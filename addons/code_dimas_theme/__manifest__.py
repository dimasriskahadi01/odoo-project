# -*- coding: utf-8 -*-
{
    'name': "Dimas Theme",

    'summary': """
        Merupakan modul untuk menambah theme
    """,

    'description': """
        Merupakan modul untuk menambah theme
    """,

    'author': "Dimas RiskaHadi",
    'website': "http://www.indonesian-aerospace.com",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/14.0/odoo/addons/base/data/ir_module_category_data.xml
    # for the full list
    'category': 'Uncategorized',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['base','website'],

    # always loaded
    'data': [
        # 'security/ir.model.access.csv',
        'assets/assets.xml',
    ],
    "qweb": [
        "static/src/xml/*.xml",
    ],
    'application': True,
    'application': False,
    'auto_install': False,
}
