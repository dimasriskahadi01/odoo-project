import odoo
import json

from odoo import http
from odoo.http import request, Response

class TestController (http.Controller):
    @http.route('/api/dimas/testing/some_json', auth='public', method=['GET'])
    def some_json(self):
        data =[];
        header_json = {'Content-Type':'application/json'}
        partner = request.env['res.partner'].sudo().search([])
        for obj in partner:
            data.append({
                'name' : obj.display_name,
                'company_name': obj.company_name,
                'contact_address': obj.contact_address
            })

        res = {'data':data}
        return Response(json.dumps(res),headers=header_json)
    
    @http.route('/api/dimas/testing/any_json', auth='public', method=['GET'])
    def any_json(self):
        data =[];
        header_json = {'Content-Type':'application/json'}
        products = request.env['product.product'].sudo().search([])
        for obj in products:
            data.append({
                'name' : obj.name,
            })

        res = {'data':data}
        return Response(json.dumps(res),headers=header_json)