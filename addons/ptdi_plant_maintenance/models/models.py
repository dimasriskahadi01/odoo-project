from odoo import api, fields, models

class PmEquipment(models.Model):
    _inherit = "product.template"

    f_equipment = fields.Boolean(string="Is Equipment")


class PmComponent(models.Model):
    _inherit = "product.template"

    f_component = fields.Boolean(string="Is Component")

class PmTools(models.Model):
    _inherit = "product.template"

    f_tool = fields.Boolean(string="Is Tools")

class PmRequest(models.Model):
    _name = "pm_ptdi.request"
    _description = "Permintaan Plant Maintenance"

    notification = fields.Integer(string="Notification")
    notification_status = fields.Selection(
        [("OSNO", "OSNO"), ("OPN", "OPEN"),("CLS", "CLOSE")],
        string="Status",
        default="OSNO")
