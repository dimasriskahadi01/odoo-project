# -*- coding: utf-8 -*-
from odoo import http


class PtdiPlantMaintenance(http.Controller):
    @http.route('/ptdi_plant_maintenance/ptdi_plant_maintenance/', auth='public')
    def index(self, **kw):
        return "Hello, world"

#     @http.route('/ptdi_plant_maintenance/ptdi_plant_maintenance/objects/', auth='public')
#     def list(self, **kw):
#         return http.request.render('ptdi_plant_maintenance.listing', {
#             'root': '/ptdi_plant_maintenance/ptdi_plant_maintenance',
#             'objects': http.request.env['ptdi_plant_maintenance.ptdi_plant_maintenance'].search([]),
#         })

#     @http.route('/ptdi_plant_maintenance/ptdi_plant_maintenance/objects/<model("ptdi_plant_maintenance.ptdi_plant_maintenance"):obj>/', auth='public')
#     def object(self, obj, **kw):
#         return http.request.render('ptdi_plant_maintenance.object', {
#             'object': obj
#         })